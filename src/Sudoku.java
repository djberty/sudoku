import sudoku.*;

public class Sudoku {
	
	protected static SudokuExceptionHandler exceptionHandler = new SudokuExceptionHandler();
	
	public static void main(String[] args) {
		try {
			Game game = new Game();
			game.load();
			game.run();
		}
		catch (SudokuException e) {
			SudokuExceptionHandler.handle(e, e.getMessage());
		}
	}
}
