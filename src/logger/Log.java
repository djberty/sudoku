package logger;
import java.util.ArrayList;

public class Log {
	
	private static ArrayList<String> messages;
	
	static {
        messages = new ArrayList<String>();
    }

	public static void log(String message) {
		messages.add(message);
	}
	
	public static void log(String message, Boolean display) {
		messages.add(message);
		if (display) {
			display();
		}
	}
	
	public static void display() {
		int size = messages.size();
		for (int idx = 0; idx < size; idx++) {
			System.out.println((idx + 1) + " : " + messages.get(idx));
		}
	}
}
