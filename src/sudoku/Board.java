package sudoku;

public class Board implements Cloneable {
	
	private Cell[][] cells = new Cell[9][9]; // Initialize cells, 9 * 9 = 81
	private Column[] columns;
	private Row[] rows;
	private Box[] boxes;
	private int curr_row = 0;
	private int curr_col = 0;
	
	public Board() {
		this.initialise();
	}
	
	private void initialise() {
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 9; y++) {
				this.cells[x][y] = new Cell();
			}
		}
	}
	
	public Object clone() {
		
		// TODO implement cloning
		try {
			Object clone = super.clone();
			return clone;
		}
		catch(CloneNotSupportedException e) {
			return null;
		}
	}
	
	public void reset() {
		this.curr_row = 0;
		this.curr_col = 0;
	}
	
	public int rowIndex() {
		return this.curr_row;
	}
	
	public int columnIndex() {
		return this.curr_col;
	}
	
	public Cell getCell(int row, int col) {
		try {
			return this.cells[row][col];
		}
		catch(Exception e) {
			return null;
		}
	}
	
	public int value() {
		Cell cell = this.getCell(this.curr_row, this.curr_col);
		return cell.getValue();
	}
	
	public Cell current() {
		return this.getCell(this.curr_row, this.curr_col);
	}
	
	public Cell next() {
		if (this.curr_col + 1 < this.cells[this.curr_row].length) {
			if (this.cells[this.curr_row][this.curr_col + 1] != null) {
				this.curr_col++;
			}
		}
		else if (this.curr_row + 1 < this.cells.length) {
			if (this.cells[this.curr_row + 1][0] != null) {
				this.curr_row++;
				this.curr_col = 0;
			}
		}
		else {
			return null;
		}
		return this.getCell(this.curr_row, this.curr_col);
	}
	
	public Boolean hasNext() {
		if (this.curr_col + 1 < this.cells[this.curr_row].length) {
			if (this.cells[this.curr_row][this.curr_col + 1] != null) {
				return true;
			}
		}
		else if (this.curr_row + 1 < this.cells.length) {
			if (this.cells[this.curr_row + 1][0] != null) {
				return true;
			}
		}
		return false;
	}
	
	public Cell previous() {
		if (this.cells[this.curr_row][this.curr_col - 1] != null) {
			this.curr_col--;
		}
		else if (this.cells[this.curr_row - 1][8] != null) {
			this.curr_row--;
			this.curr_col = 8;
		}
		return this.getCell(this.curr_row, this.curr_col);

	}
	
	public Boolean hasPrevious() {
		if (this.cells[this.curr_row][this.curr_col - 1] != null) {
			return true;
		}
		else if (this.cells[this.curr_row - 1][8] != null) {
			return true;
		}
		return false;
	}
	
	public Cell left() {
		return this.row().previous();
	}
	
	public Boolean hasLeft() {
		return this.row().hasPrevious();
	}
	
	public Cell right() {
		return this.row().next();
	}
	
	public Boolean hasRight() {
		return this.row().hasNext();
	}
	
	public Cell up() {
		return this.column().previous();
	}
	
	public Boolean hasUp() {
		return this.column().hasPrevious();
	}
	
	public Cell down() {
		return this.column().next();
	}
	
	public Boolean hasDown() {
		return this.column().hasNext();
	}
	
	public Row row() {
		try {
			return this.getRow(this.curr_row);
		}
		catch (SudokuException e) {
			return null;
		}
	}
	
	public Row getRow(int row_number) throws SudokuException {
		if (row_number > -1 && row_number < 9) {
			if (this.rows[row_number] != null) {
				return this.rows[row_number];
			}
			Row row = new Row(this, row_number);
			this.rows[row_number] = row;
			return row;
		}
		throw new SudokuException("Row out of range: " + row_number);
	}
	
	public Column column() {
		try {
			return this.getColumn(this.curr_col);
		}
		catch (SudokuException e) {
			return null;
		}
	}
	
	public Column getColumn(int col_number) throws SudokuException {
		if (col_number > -1 && col_number < 9) {
			if (this.columns[col_number] != null) {
				return this.columns[col_number];
			}
			Column column = new Column(this, col_number);
			this.columns[col_number] = column;
			return column;
		}
		throw new SudokuException("Column out of range: " + col_number);
	}
	
	public Box box() {
		try {
			int box_index = Box.findBoxIndex(this.curr_row, this.curr_col);
			return this.getBox(box_index);
		}
		catch (SudokuException e) {
			return null;
		}
	}
	
	public Box getBox(int box_index) throws SudokuException {
		if (box_index > -1 && box_index < 9) {
			if (this.boxes[box_index] != null) {
				return this.boxes[box_index];
			}
			Box box = new Box(this, box_index);
			this.boxes[box_index] = box;
			return box;
		}
		throw new SudokuException("Box out of range: " + box_index);
	}
	
	public Boolean solved() {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; row < 9; row++) {
				if (this.cells[row][col].getValue() == 0) {
					return false;
				}
			}
		}
		return true;
	}
}
