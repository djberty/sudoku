package sudoku;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;

//VS4E -- DO NOT REMOVE THIS LINE!
public class BoardFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private static final int COLUMN_ROW_SPLIT_PIXELS = 0;
	
	private JTextField[][] fields = new JTextField[9][9];
	private CellActionHandler cell_action_handler = new CellActionHandler();
	private MenuActionHandler menu_action_handler = new MenuActionHandler();
	
	private JMenuItem jMenuItemNew;
	private JMenuItem jMenuItemSolve;
	private JMenuItem jMenuItemLoad;
	private JMenuItem jMenuItemSave;
	private JMenuItem jMenuItemExit;
	private JMenu jMenu;
	private JMenuBar jMenuBar;
	
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	
	public BoardFrame() {
		installLnF();
		initComponents();
	}

	private void initComponents() {
		setTitle("Sudoku");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setLayout(new GroupLayout());
		setJMenuBar(getTheMenuBar());
		setSize(479, 381);
	}

	private JMenuBar getTheMenuBar() {
		if (jMenuBar == null) {
			jMenuBar = new JMenuBar();
			jMenuBar.add(getJMenu());
		}
		return jMenuBar;
	}

	private JMenu getJMenu() {
		if (jMenu == null) {
			jMenu = new JMenu();
			jMenu.setText("Game");
			jMenu.add(getJMenuItemNew());
			jMenu.add(getJMenuItemSolve());
			jMenu.add(getJMenuItemLoad());
			jMenu.add(getJMenuItemSave());
			jMenu.add(getJMenuExit());
		}
		return jMenu;
	}
	
	private JMenuItem getJMenuItemNew() {
		if (jMenuItemNew == null) {
			jMenuItemNew = new JMenuItem();
			jMenuItemNew.setText("New");
			jMenuItemNew.addActionListener(menu_action_handler);
		}
		return jMenuItemNew;
	}
	
	private JMenuItem getJMenuItemSolve() {
		if (jMenuItemSolve == null) {
			jMenuItemSolve = new JMenuItem();
			jMenuItemSolve.setText("Solve");
			jMenuItemSolve.addActionListener(menu_action_handler);
		}
		return jMenuItemSolve;
	}
	
	private JMenuItem getJMenuItemLoad() {
		if (jMenuItemLoad == null) {
			jMenuItemLoad = new JMenuItem();
			jMenuItemLoad.setText("Load");
			jMenuItemLoad.addActionListener(menu_action_handler);
		}
		return jMenuItemLoad;
	}
	
	private JMenuItem getJMenuItemSave() {
		if (jMenuItemSave == null) {
			jMenuItemSave = new JMenuItem();
			jMenuItemSave.setText("Save");
			jMenuItemSave.addActionListener(menu_action_handler);
		}
		return jMenuItemSave;
	}

	private JMenuItem getJMenuExit() {
		if (jMenuItemExit == null) {
			jMenuItemExit = new JMenuItem();
			jMenuItemExit.setText("Exit");
			jMenuItemExit.addActionListener(menu_action_handler);
		}
		return jMenuItemExit;
	}
	
	public void buildCells() {
		int horiz = 5, vert = 5, size = 35, min = 10;
		int row_incr = 0, col_incr = 0;
		Leading leading_h, leading_v;
		for (int row = 0; row < 9; row++) {
			row_incr = 0;
			if (row % 3 == 0) {
				row_incr = COLUMN_ROW_SPLIT_PIXELS;
			}
			for (int col = 0; col < 9; col++) {
				col_incr = COLUMN_ROW_SPLIT_PIXELS;
				if (col % 3 == 0) {
					col_incr = 1;
				}
				JTextField text_field = this.buildTextField();
				this.fields[row][col] = text_field;
				leading_h = new Leading(horiz + (col * size) + col_incr, size, min, min);
				leading_v = new Leading(vert + (row * size) + row_incr, size, min, min);
				add(text_field, new Constraints(leading_h, leading_v));
			}
		}
	}
	
	private JTextField buildTextField() {
		JTextField field = new JTextField();
		field.setFont(new Font("Dialog", Font.PLAIN, 18));
		field.setHorizontalAlignment(SwingConstants.CENTER);
		field.addActionListener(cell_action_handler);
		return field;
	}
	
	public void display() {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void populateCells(Board board) {
		for (int row = 0; row < 9; row++) {
			for (int col = 0; col < 9; col++) {
				Cell cell = board.getCell(row, col);
				if (cell.getValue() > 0) {
					this.fields[row][col].setText(Integer.toString(cell.getValue()));
				}
				else {
					this.fields[row][col].setText("");
				}
				this.fields[row][col].setEditable(!cell.isFixed());
			}
		}
	}

	private static void installLnF() {
		try {
			UIManager.setLookAndFeel(PREFERRED_LOOK_AND_FEEL);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}
}
