package sudoku;

public class Box extends CellGroup {
	
	private int box_index;
	
	public Box(Board board, int box_index) {
		super(board);
		this.box_index = box_index;
	}
	
	//TODO implement cloning
	
	protected void populate() {
		
	}
	
	public int getBoxIndex() {
		return this.box_index;
	}
	
	public static int findBoxIndex(int board_row, int board_col) {
		
		// 1.) row: 0 -> 2, column: 0 -> 2
		// 2.) row: 0 -> 2, column: 3 -> 5
		// 3.) row: 0 -> 2, column: 6 -> 8
		if (board_row < 3) {
			if (board_col < 3) {
				return 0;
			}
			else if (board_col < 6) {
				return 1;
			}
			else if (board_col < 9) {
				return 2;
			}
		}
		// 4.) row: 3 -> 5, column: 0 -> 2
		// 5.) row: 3 -> 5, column: 3 -> 5
		// 6.) row: 3 -> 5, column: 6 -> 8
		else if (board_row < 6) {
			if (board_col < 3) {
				return 3;
			}
			else if (board_col < 6) {
				return 4;
			}
			else if (board_col < 9) {
				return 5;
			}
		}
		// 7.) row: 6 -> 8, column: 0 -> 2
		// 8.) row: 6 -> 8, column: 3 -> 5
		// 9.) row: 6 -> 8, column: 6 -> 8
		else if (board_row < 9) {
			if (board_col < 3) {
				return 6;
			}
			else if (board_col < 6) {
				return 7;
			}
			else if (board_col < 9) {
				return 8;
			}
		}
		return -1;
	}
}
