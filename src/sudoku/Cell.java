package sudoku;

import java.util.List;
import java.util.ArrayList;

public class Cell implements Cloneable {
	
	private int value = 0;
	private Boolean fixed = false;
	private List<Integer> possibles = new ArrayList<Integer>();
	
	public Object clone() {
		try {
			Object clone = super.clone();
			return clone;
		}
		catch(CloneNotSupportedException e) {
			return null;
		}
	}
	
	final public int getValue() {
		return this.value;
	}
	
	final public void setValue(int val) {
		if (this.valueInRange(val) && this.hasPossibleValue(val)) {
			this.value = val;
			this.removePossibleValue(this.value);
			return;
		}
		// TODO add error handling
	}
	
	final public Boolean isFixed() {
		return this.fixed;
	}
	
	final public void setFixed(Boolean status) {
		this.fixed = status;
	}
	
	final public void unsetValue() {
		if (!this.fixed) {
			this.addPossibleValue(this.value);
			this.value = 0;
		}
	}
	
	final public void addPossibleValue(int value) {
		if (!this.fixed) {
			if (this.possibles.indexOf(value) != -1) {
				this.possibles.add(value);
			}
		}
	}
	
	final public void removePossibleValue(int value) {
		int idx = this.possibles.indexOf(value);
		if (idx > -1) {
			this.possibles.remove(idx);
		}
	}
	
	final public List<Integer> getPossibleValues() {
		return this.possibles;
	}
	
	final public Boolean hasPossibleValue(int value) {
		return true;
		// TODO Activate this when we have possibles population working correctly
		// return this.possibles.contains(value);
	}
	
	/**
	 * Determines if the passed value is allowed
	 * @param val value to examine
	 * @return
	 */
	final private Boolean valueInRange(int val) {
		if (val > 0 && val < 10) {
			return true;
		}
		return false;
	}
	
	/**
	 * For the given cell, using the given board, determine and set the possible values.
	 * @param cell
	 * @param board
	 */
	final public static void calculatePossibles(Cell cell, Board board) {
		board.row();
	}
}
