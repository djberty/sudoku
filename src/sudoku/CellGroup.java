package sudoku;

public abstract class CellGroup {
	
	protected Board board;
	protected Cell[] cells = new Cell[9];
	protected int curr_index = 0;
	
	//TODO implement cloning
	
	abstract void populate();
	
	public CellGroup(Board board) {
		this.board = board;
		this.populate();
	}
	
	final public Board getBoard() {
		return this.board;
	}
	
	public void reset() {
		this.curr_index = 0;
	}
	
	public Cell current() {
		return this.cells[this.curr_index];
	}
	
	public Cell next() {
		this.curr_index++;
		if (this.cells[this.curr_index] != null) {
			return this.cells[this.curr_index];
		}
		return null;
	}
	
	public Boolean hasNext() {
		if (this.curr_index < 8) {
			return true;
		}
		return false;
	}
	
	public Cell nextWithoutValue() {
		Cell cell;
		for (int idx = 0; idx < this.cells.length; idx++) {
			cell = this.cells[idx];
			if (cell.getValue() == 0) {
				this.curr_index = idx;
				return cell;
			}
		}
		return null;
	}
	
	public Cell previous() {
		this.curr_index--;
		if (this.cells[this.curr_index] != null) {
			return this.cells[this.curr_index];
		}
		return null;
	}
	
	public Boolean hasPrevious() {
		if (this.curr_index > 0) {
			return true;
		}
		return false;
	}
	
	public Cell previousWithoutValue() {
		Cell cell;
		for (int idx = this.curr_index; idx > -1; idx--) {
			cell = this.cells[idx];
			if (cell.getValue() == 0) {
				this.curr_index = idx;
				return cell;
			}
		}
		return null;
	}
	
	final public Boolean hasCellValue(Cell cell) {
		return this.hasCellValue(cell.getValue());
	}
	
	final public Boolean hasCellValue(int value) {
		for (Cell cell : this.cells) {
			if (cell.getValue() == value) {
				return true;
			}
		}
		return false;
	}
	
	
}
