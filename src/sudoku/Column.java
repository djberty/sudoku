package sudoku;

public class Column extends CellGroup {
	
	private int column_index;
	
	public Column(Board board, int column_index) {
		super(board);
		this.column_index = column_index;
	}
	
	protected void populate() {
		for (int idx = 0; idx < 9; idx++) {
			this.cells[idx] = this.getBoard().getCell(idx, board.columnIndex());
		}
	}
	
	public int getColumnIndex() {
		return this.column_index;
	}
}
