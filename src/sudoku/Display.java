package sudoku;

import logger.Log;

public class Display {
	
	private Board board;
	
	private BoardFrame frame = new BoardFrame();
	
	public Display(Board board) {
		this.board = board;
	}
	
	public void output() {
		this.board.reset();
		int count = 1;
		Cell cell = this.board.current();
		System.out.println();
		System.out.println("-------------------------------");
		System.out.print("|");
		while (count < 82) {
			if (cell.getValue() > 0) {
				System.out.print(" " + cell.getValue() + " ");
			}
			else {
				System.out.print("   ");
			}
			if (count % 9 == 0) {
				System.out.print("|");
				System.out.println();
			}
			if (count == 27 || count == 54) {
				System.out.println("-------------------------------");
			}
			count++;
			if (count % 3 == 1 && count < 81) {
				System.out.print("|");
			}
			cell = this.board.next();
		}
		System.out.println("-------------------------------");
		
		System.out.println();
		System.out.println("Log Messages: ");
		Log.display();
		this.window();
	}
	
	public void window() {
		
		//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	buildGUI(frame);
            	populate(frame, board);
            	showGUI(frame);
            }
        });
	}
	
	private void buildGUI(BoardFrame frame) {
		frame.buildCells();
	}
	
	private void populate(BoardFrame frame, Board board) {
		frame.populateCells(board);
	}
	
	private void showGUI(BoardFrame frame) {
        frame.display();
	}
}
