package sudoku;

import java.io.*;
import java.util.Scanner;

public class FileData {
	
	private String data;
	private String filename;
	
	public FileData(String filename) {
		this.filename = filename;
		try {
			this.read();
		}
		catch (IOException e) {
			System.out.println("Man... I just could not read that file: " + this.filename);
			System.exit(0);
		}
	}
	
	private void read() throws IOException {
		File file = new File(this.filename);
		Scanner scanner = new Scanner(file);
		try {
			StringBuilder contents = new StringBuilder();
			while (scanner.hasNextLine()){
				contents.append(scanner.nextLine());
			}
			this.data = contents.toString();
		}
		finally {
			scanner.close();
		}
	}
	
	public String getData() {
		return this.data.replace('.', '0');
	}
}
