package sudoku;

import logger.Log;

public class Game {
	
	private Board board;
	
	public Game() throws SudokuException {
		this.board = new Board();
	}
	
	public void load() throws SudokuException {
		
		Cell cell;
		FileData file_data = new FileData("data\\puzzle1.txt");
		char[] data = file_data.getData().toCharArray();
		
		if (data.length < 80) {
			throw new SudokuException("Not enough data in the file.");
		}
		
		this.board.current().setValue(Integer.parseInt(String.valueOf(data[0])));
		
		int count = 1; 
		while (this.board.hasNext()) {
			cell = this.board.next();
			int value = Integer.parseInt(String.valueOf(data[count]));
			cell.setValue(value);
			if (value > 0) {
				cell.setFixed(true);
			}
			else {
				cell.setFixed(false);
			}
			count++;
		}
	}
	
	public void run() {
		Log.log("Running...");
		Display display = new Display(this.board);
		Solver solver = new Solver(this.board);
		Log.log("Solving...");
		solver.solve();
		Log.log("Done.");
		display.output();
	}
	
	public void save() {
		
	}
	
	public void exit() {
		
	}
}
