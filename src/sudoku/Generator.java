package sudoku;

public class Generator {
	
	private Board board;
	
	public Generator() {
		this.board = new Board();
	}
	
	public Generator(Board board) {
		this.board = board;
	}
	
	public void generate() {
		
	}
	
	public Board getBoard() {
		return this.board;
	}
}
