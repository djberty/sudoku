package sudoku;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JMenuItem;

public class MenuActionHandler implements ActionListener, ItemListener {
	
	public void actionPerformed(ActionEvent e) {
		JMenuItem field = (JMenuItem) e.getSource();
		String field_text = field.getText();
		if (field_text == "New") {
			this.newClick();
		}
		else if (field_text == "Solve") {
			this.solveClick();
		}
		else if (field_text == "Save") {
			this.saveClick();
		}
		else if (field_text == "Load") {
			this.loadClick();
		}
		else if (field_text == "Exit") {
			this.exitClick();
		}
		else {
			System.out.println("Unknown menu item action triggered: " + field.getText());
		}
	}

	public void itemStateChanged(ItemEvent arg0) {}
	
	private void newClick() {
		System.out.println("Menu Item Clicked: New");
	}
	
	private void solveClick() {
		System.out.println("Menu Item Clicked: Solve");
	}
	
	private void saveClick() {
		System.out.println("Menu Item Clicked: Save");
	}
	
	private void loadClick() {
		System.out.println("Menu Item Clicked: Load");
	}
	
	private void exitClick() {
		System.out.println("Menu Item Clicked: Exit");
	}
}
