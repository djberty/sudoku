package sudoku;

public class Row extends CellGroup {
	
	private int row_index;
	
//TODO implement cloning
	
	public Row(Board board, int row_index) {
		super(board);
		this.row_index = row_index;
	}
	
	protected void populate() {
		for (int idx = 0; idx < 9; idx++) {
			this.cells[idx] = this.getBoard().getCell(board.rowIndex(), idx);
		}
	}
	
	public int getRowIndex() {
		return this.row_index;
	}
}