package sudoku;

public class Solver {
	
	private Board board;
	
	public Solver(Board board) {
		this.board = board;
	}
	
	final public Boolean solve() {
		
		return true;
	}
	
	final public void updatePossibles() {
		
		Boolean has_possibles = true;
		
		while (has_possibles) {
		
			Row candidate_row = board.row();
			Column candidate_col = board.column();
			Box candidate_box = board.box();
			
			Cell candidate_cell = candidate_row.nextWithoutValue();
			
			has_possibles = false;
			for (int value = 1; value <= 9; value++) {
				if (!candidate_row.hasCellValue(value)) {
					if (!candidate_col.hasCellValue(value)) {
						if (!candidate_box.hasCellValue(value)) {
							has_possibles = true;
							candidate_cell.addPossibleValue(value);
						}
					}
				}
			}
		}
	}
}
