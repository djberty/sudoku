package sudoku;

public class SudokuException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8984817485902848032L;
	
	public SudokuException(String msg) {
		System.out.println("EXCEPTION: " + msg);
		System.out.println(this.toString());
	}
}
