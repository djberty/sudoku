package sudoku;

public class Validator {
	
	private Board orig_board;
	private Board work_board;
	
	public Validator(Board board) {
		this.orig_board = board;
		this.work_board = (Board) board.clone();
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isValid() {
		
		Cell blank_cell = this.board.row().nextWithoutValue();
		if (blank_cell != null) {
			
		}
		else {
			if (this.board) {
				return false;
			}
			return true;
		}
		
		/*
		 * 
valid_puzzle(grid):
     try to find a blank cell
     if blank cell found then
         for each value 1..9
             if value does not already occur in row/column/box of cell
                fill cell with value
                if( !valid_puzzle(grid) ) return false;
                clear cell
         return true
     else
         if is not original solution
             return false
         return true 
		 */
	}
}
